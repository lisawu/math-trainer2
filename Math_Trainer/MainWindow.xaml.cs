﻿using System;
using System.Windows;

namespace MathTrainerVC19
{
    public partial class MainWindow : Window
    {
        //Trainer mTrainer = new Trainer();
        public MainWindow()
        {
            InitializeComponent();
            this.ResizeMode = System.Windows.ResizeMode.CanMinimize;
        }

        public void ErrorTextLabelSet(string tData)
        {
            this.ErrorTextLabel.Content = tData;
        }

        public void SetStartButtonState(bool flag)
        {
            if (flag) { this.ButtonStart.IsEnabled = true; }
            else { this.ButtonStart.IsEnabled = false; }
        }

//         protected override void OnClosing(CancelEventArgs e)
//         {
//             MessageBoxResult result = MessageBox.Show("Do you really want to quit?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
//             if (result == MessageBoxResult.No) { e.Cancel = true; }
//         }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            //mTrainer.SetExerciseLabel("test");
            // Switch to Trainer Window
            var trainerWindow = new Trainer();
            trainerWindow.Show();

            // Check for null and close current Window
            if (System.Windows.Application.Current.MainWindow != null)
                this.Close();
        }

        private void IsAdditionChecked(object sender, RoutedEventArgs e)
        {
            Handler._UsingAddition = true;
        }

        private void IsSubtractionChecked(object sender, RoutedEventArgs e)
        {
            Handler._UsingSubtraction = true;
        }

        private void IsMultiplicationChecked(object sender, RoutedEventArgs e)
        {
            Handler._UsingMultiplication = true;
        }

        private void IsDivisionChecked(object sender, RoutedEventArgs e)
        {
            Handler._UsingDivision = true;
        }

        private void EasyStartTime(object sender, RoutedEventArgs e)
        {
            Handler.Set_EasyStartTime(true);
        }

        private void MediumStartTime(object sender, RoutedEventArgs e)
        {
            Handler.Set_MidStartTime(true);
        }

        private void HardStartTime(object sender, RoutedEventArgs e)
        {
            Handler.Set_HardStartTime(true);
        }

        private void TxtBox_HighestResult_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs arg)
        {
            ExceptionManager.Control.AdvancedInputCheck(TxtBox_HighestResult.Text);
        }

        private void textboxTxtInLimitExercises_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs arg)
        {
            Handler._MaxExercises = ExceptionManager.Control.AdvancedInputCheck(textboxTxtInLimitExercises.Text);
        }

        private void ConfigTimeLimit_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs arg)
        {
            ExceptionManager.Control.AdvancedInputCheck(textboxTxtInLimitExercises.Text);
        }
    }
}
