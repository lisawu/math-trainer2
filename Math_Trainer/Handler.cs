﻿using System;

namespace MathTrainerVC19
{
    class Handler
    {
        protected static bool AdditionFlag;
        protected static bool SubtractionFlag;
        protected static bool MultiplicationFlag;
        protected static bool DivisionFlag;

        protected static bool StartTimeEasyFlag;
        protected static bool StartTimeMidFlag;
        protected static bool StartTimeHardFlag;

        protected static int tMaxExerciseResult;
        protected static int tMaxExercises;

        public static int max_per_operand = 99;
        private static int tResult;

        public static bool Set_EasyStartTime(bool flag)
        {
            // flush vars....
            Trainer.initial_start_time += 15; // ok
            StartTimeEasyFlag = flag;
            return flag;
        }

        public static bool Set_MidStartTime(bool flag)
        {
            Trainer.initial_start_time += 10;
            StartTimeMidFlag = flag;
            return flag;
        }

        public static bool Set_HardStartTime(bool flag)
        {

            Trainer.initial_start_time += 5;
            StartTimeHardFlag = flag;
            return flag;
        }


        /// SETTERS and GETTERS
        public static bool _UsingAddition { get => AdditionFlag; set => AdditionFlag = value; }

        public static bool _UsingSubtraction { get => SubtractionFlag; set => SubtractionFlag = value; }

        public static bool _UsingMultiplication { get => MultiplicationFlag; set => MultiplicationFlag = value; }

        public static bool _UsingDivision { get => DivisionFlag; set => DivisionFlag = value; }

        public static int _MaxExerciseResult { get => tMaxExerciseResult; set => tMaxExerciseResult = value; }

        public static int _MaxExercises { get => tMaxExercises; set => tMaxExercises = value; }

        public static int _Result { get => tResult; set => tResult = value; }

        public static int MaxOperands()
        {
            int num_operands = 0;

            if (Handler._UsingAddition)
                num_operands += 1;

            else if (Handler._UsingSubtraction)
                num_operands += 1;

            else if (Handler._UsingMultiplication)
                num_operands += 1;

            else if (Handler._UsingDivision)
                num_operands += 1;

            return num_operands;
        }

    }
}