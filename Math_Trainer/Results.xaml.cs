﻿using System;
using System.Windows;

namespace MathTrainerVC19
{
    /// <summary>
    /// Interaction logic for Results.xaml
    /// </summary>
    public partial class Results : Window
    {
        public Results()
        {
            InitializeComponent();
            this.ResizeMode = System.Windows.ResizeMode.CanMinimize;
        }

        private void BtnNewTraining_Click(object sender, RoutedEventArgs e)
        {
            // Switch to configuration window
            var configWindow = new MainWindow();
            configWindow.Show();

            // Check for null and close current Window
            if (System.Windows.Application.Current.MainWindow != null)
                this.Close();
        }

        private void BtnRestart_Click(object sender, RoutedEventArgs e)
        {
            // Switch back to saved training session
            var trainerWindow = new Trainer(); // WIP
            trainerWindow.Show();

            // Check for null and close current Window
            if (System.Windows.Application.Current.MainWindow != null)
                this.Close();
        }

        private void BtnSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            // WIP
        }
    }
}
