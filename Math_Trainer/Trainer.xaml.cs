﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace MathTrainerVC19
{
    public partial class Trainer : Window
    {
        DispatcherTimer _timer;
        TimeSpan _time;
        public static int initial_start_time = 0;

        //public void SetExerciseLabel(string tData)
        //{
        //    this.lbl_Exercise.Content = tData;
        //}

        public Trainer() // this isn't C++ no prototypes needed
        {
            InitializeComponent();


            /// Timer
            _time = TimeSpan.FromSeconds(initial_start_time);

            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                lblTime.Content = _time.ToString("c");
                if (_time == TimeSpan.Zero)
                {
                    _timer.Stop();
                    BtnShowResults_Click(_timer, null);
                }
                _time = _time.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);

            _timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            lblTime.Content = DateTime.Now.ToLongTimeString();
            lbl_Exercise.Content = "`test";
        }

        private void BtnShowResults_Click(object sender, RoutedEventArgs e)
        {
            // Switch back to saved training session
            var resultsWindow = new Results(); // WIP
            resultsWindow.Show();

            // Check for null and close current Window
            if (System.Windows.Application.Current.MainWindow != null)
                this.Close();
        }

        private void BtnNewTraining_Click(object sender, RoutedEventArgs e)
        {
            // Switch back to saved training session
            var trainerWindow = new Trainer(); // WIP
            trainerWindow.Show();

            // Check for null and close current Window
            if (System.Windows.Application.Current.MainWindow != null)
                this.Close();
        }

        private void Result_Input_Box_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {

        }

        private void lbl_Exercise_Initialized(object sender, EventArgs e)
        {
            string exercise_caption = string.Empty;
            int left_operand;
            int right_operand;
            int target_operand;

            lbl_Exercise.Content = "`test";

            int t_max_operands = Handler.MaxOperands() + 1;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                Dispatcher.Invoke(new Action(() =>
                {

                    do
                    {
                        Random rnd = new Random();
                        left_operand = rnd.Next(1, Handler.max_per_operand);
                        right_operand = rnd.Next(1, Handler.max_per_operand);
                        target_operand = rnd.Next(1, t_max_operands);

                    } while (left_operand + right_operand >= Handler._MaxExerciseResult);

                    switch (target_operand)
                    {
                        case 1:
                            Handler._Result = left_operand + right_operand;
                            exercise_caption = left_operand + " + " + right_operand;
                            break;
                        case 2:
                            Handler._Result = left_operand - right_operand;
                            exercise_caption = left_operand + " - " + right_operand;
                            break;  
                        case 3:
                            Handler._Result = left_operand * right_operand;
                            exercise_caption = left_operand + " x " + right_operand;
                            break;
                        case 4:
                            Handler._Result = left_operand / right_operand;
                            exercise_caption = left_operand + " : " + right_operand;
                            break;
                        default:
                            break;
                    }
                    lbl_Exercise.Content = "`test";
                }));
            });

            
        }

        private void lbl_Exercise_Initialized_1(object sender, EventArgs e)
        {

        }
    }
}
